@extends('layouts.layout')
@section('konten')

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>get in <span>touch</span></h1>
    <span class="title-bg">contact</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Left Side Starts -->
            <div class="col-12 col-lg-4">
                <h3 class="text-uppercase custom-title mb-0 ft-wt-600 pb-3">Don't be shy !</h3>
                <p class="open-sans-font mb-3">Feel free to get in touch with me. I am always open to discussing new projects, creative ideas or opportunities to be part of your visions.</p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-envelope-open position-absolute"></i>
                    <span class="d-block">mail me</span>bagusbayusurya@gmail.com
                </p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-phone-square position-absolute"></i>
                    <span class="d-block">call me</span>+6283117509074
                </p>
                <ul class="social list-unstyled pt-1 mb-5">
                    <li class="facebook"><a title="Facebook" href="https://mobile.facebook.com/profile.php?ref_component=mfreebasic_home_header&ref_page=%2Fwap%2Fhome.php&refid=7&_rdr" target="_blang"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="instagram"><a title="Instagram" href="https://www.instagram.com/_wahbayu21/" target="_blang"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="youtube"><a title="Youtube" href="https://www.youtube.com/channel/UCYWxwf_1jxsP2b07ddX91dg" target="_blang"><i class="fa fa-youtube"></i></a>
                    </li>
                    <li class="whatsapp"><a title="WhatsApp" href="https://wa.wizard.id/c29daf" target="_blang"><i class="fa fa-whatsapp"></i></a>
                    </li>
                </ul>
            </div>
            <!-- Left Side Ends -->
            <!-- Contact Form Starts -->
            <div class="col-12 col-lg-8">
                <form class="contactform" method="post" action="http://slimhamdi.net/tunis/dark/php/process-form.php">
                    <div class="contactform">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <input type="text" name="name" placeholder="YOUR NAME">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="email" name="email" placeholder="YOUR EMAIL">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="text" name="subject" placeholder="YOUR SUBJECT">
                            </div>
                            <div class="col-12">
                                <textarea name="message" placeholder="YOUR MESSAGE"></textarea>
                                <button type="submit" class="btn btn-contact">Send Message</button>
                            </div>
                            <div class="col-12 form-message">
                                <span class="output_message text-center font-weight-600 text-uppercase"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Contact Form Ends -->
        </div>
    </div>

</section>

</body>

<!-- Mirrored from slimhamdi.net/tunis/dark/contact.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:57:28 GMT -->
</html>
@endsection