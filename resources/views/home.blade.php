@extends('layouts.layout')
@section('konten')

<!-- Main Content Starts -->
<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                <img src="img/2.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="Wah Bayu" />
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Om Swastiastu</h6>
                <h1 class="text-uppercase poppins-font"><span>Saya</span> I Gusti Ngurah Bagus Bayu Surya</h1>
                <p class="open-sans-font">I am a student at Ganesha University of Education taking the Informatics Engineering Education Study Program.</p>
                <a href="/about" class="btn btn-about">more about me</a>
            </div>
        </div>
    </div>
</section>
<!-- Main Content Ends -->

</body>

<!-- Mirrored from slimhamdi.net/tunis/dark/.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:57:16 GMT -->
</html>
@endsection