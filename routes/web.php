<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\ContactController;



Route::get('/', [HomeController::class,'home']);

Route::get('/about', [AboutController::class,'about']);

Route::get('/gallery', [GalleryController::class, 'gallery']);

Route::get('/contact', [ContactController::class,'contact']);
